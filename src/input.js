const inquirer = require("inquirer");

// Input constants
const ADD_EXPENSE = 1;
const REMOVE_EXPENSE = 2;
const EDIT_EXPENSE = 3;
const VIEW_LIST_EXPENSES = 4;
const VIEW_CHART_CATEGORY = 5;
const VIEW_CHART_MONTH = 6;
const QUIT = 7;

/**
 * Show main application menu
 **/
const mainMenu = async () => {
  const options = [
    {
      name: "option",
      type: "list",
      message: "Select your option",
      choices: [
        { value: ADD_EXPENSE, name: "Add expense" },
        { value: REMOVE_EXPENSE, name: "Remove expense" },
        { value: EDIT_EXPENSE, name: "Edit expense" },
        { value: VIEW_LIST_EXPENSES, name: "View list of expenses" },
        { value: VIEW_CHART_CATEGORY, name: "View chart by Category" },
        { value: VIEW_CHART_MONTH, name: "View chart by Month" },
        { value: QUIT, name: "Quit" },
      ],
    },
  ];
  // Show prompt in terminal
  return inquirer.prompt(options);
};

/**
 * Show menu to input a new expense
 **/
const inputNewExpense = () => {
  const options = [
    {
      name: "title",
      type: "input",
      message: "Type the title 📃",
      validate: async (input) => {
        return input !== "" ? true : "Please, type the title of the expense";
      },
    },
    {
      name: "amount",
      type: "input",
      message: "Type the amount 💰",
      validate: async (input) => {
        if (!isNaN(input)) {
          if (parseFloat(input) > 0) {
            return true;
          } else {
            return "Please, type a number greater than zero";
          }
        } else {
          return "Please, type a number greater than zero";
        }
      },
    },
    {
      name: "category",
      type: "input",
      message: "Type the category 🏷",
    },
    {
      name: "month",
      type: "list",
      message: "Select the month 📅",
      choices: [
        { value: 1, name: "January" },
        { value: 2, name: "February" },
        { value: 3, name: "March" },
        { value: 4, name: "April" },
        { value: 5, name: "May" },
        { value: 6, name: "June" },
        { value: 7, name: "July" },
        { value: 8, name: "August" },
        { value: 9, name: "September" },
        { value: 10, name: "October" },
        { value: 11, name: "November" },
        { value: 12, name: "December" },
      ],
    },
  ];
  // Show prompt in terminal
  return inquirer.prompt(options);
};

module.exports = {
  mainMenu,
  inputNewExpense,
  ADD_EXPENSE,
  REMOVE_EXPENSE,
  EDIT_EXPENSE,
  VIEW_LIST_EXPENSES,
  VIEW_CHART_CATEGORY,
  VIEW_CHART_MONTH,
  QUIT,
};
