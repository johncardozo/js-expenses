let Table = require("cli-table3");
const ervy = require("ervy");
let figlet = require("figlet");

const { bar, bg } = ervy;

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Nov",
  "Dec",
];

const colors = ["red", "green", "yellow", "blue", "magenta", "cyan", "white"];

/**
 * Show the expenses in a table
 * @param  {array} expenses to show
 */
const showListExpenses = (expenses) => {
  // Configure the table
  var table = new Table({
    head: ["month", "category", "title", "amount"],
  });

  // Iterate over expenses
  for (const expense of expenses) {
    // Add a new value to the table
    table.push([
      months[expense.month - 1],
      expense.category,
      expense.title,
      expense.amount,
    ]);
  }

  // Show the table
  console.log(table.toString());
};

/**
 * Show a chart of expenses grouped by month
 * @param  {array} expenses to be shown as a chart
 */
const showChartByMonth = (expenses) => {
  let expensesData = [];
  for (const expense of expenses) {
    // Get the index of the current expense month in expenses data
    const index = expensesData.findIndex(
      (d) => d.key === months[expense.month - 1]
    );

    // Verify if the month exist in expenses data
    if (index === -1) {
      // Create a new data for expenses data array
      const newData = {
        key: months[expense.month - 1],
        value: parseInt(expense.amount),
        style: bg(getRandomColor()),
        barWidth: 1,
      };
      // Add new data to expenses data array
      expensesData.push(newData);
    } else {
      // Update expense data value
      expensesData[index].value =
        expensesData[index].value + parseInt(expense.amount);
    }
  }
  // Show the chart in terminal
  console.log(bar(expensesData));
};

/**
 * Get random color for the bar chart
 */
function getRandomColor() {
  let min = Math.ceil(0);
  let max = Math.floor(colors.length);
  return colors[Math.floor(Math.random() * (max - min + 1)) + min];
}

/**
 * Show a message with big font in terminal
 * @param  {string} message to show
 */
const showBigText = (message) => {
  figlet(message, function (err, data) {
    if (err) {
      console.log("Something went wrong...");
      console.dir(err);
      return;
    }
    console.log(data);
  });
};

module.exports = {
  showListExpenses,
  showChartByMonth,
  showBigText,
};
