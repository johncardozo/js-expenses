const path = require("path");
const Datastore = require("nedb-promises");

// Get database path
const dbPath = path.join(__dirname, "../data/expenses.db");

// Get access to database
let db = Datastore.create(dbPath);

/**
 * Save an expense in database
 * @param  {object} expense to be saved
 */
const saveExpense = async (expense) => {
  await db.insert(expense);
};

/**
 * Get all expenses from database
 */
const getAllExpenses = async () => {
  return await db.find({}).sort({ month: -1 });
};

module.exports = {
  saveExpense,
  getAllExpenses,
};
