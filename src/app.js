const {
  mainMenu,
  inputNewExpense,
  ADD_EXPENSE,
  REMOVE_EXPENSE,
  EDIT_EXPENSE,
  VIEW_LIST_EXPENSES,
  VIEW_CHART_CATEGORY,
  VIEW_CHART_MONTH,
  QUIT,
} = require("./input");
const { saveExpense, getAllExpenses } = require("./data");
const { showListExpenses, showChartByMonth, showBigText } = require("./output");

const main = async () => {
  // Loop while user doesn't quit
  let mainOption = -1;
  while (mainOption !== QUIT) {
    // Ask main option
    let choice = await mainMenu();
    mainOption = choice.option;

    // Check selected option
    switch (mainOption) {
      case ADD_EXPENSE:
        // Show new expense menu
        let newExpense = await inputNewExpense();

        // Save new expense
        let result = await saveExpense(newExpense);

        break;
      case REMOVE_EXPENSE:
        break;
      case EDIT_EXPENSE:
        break;
      case VIEW_LIST_EXPENSES:
        {
          // Get all expenses
          let expenses = await getAllExpenses();

          // Show the expenses
          showListExpenses(expenses);
        }
        break;
      case VIEW_CHART_CATEGORY:
        break;
      case VIEW_CHART_MONTH:
        {
          // Get all expenses
          let expenses = await getAllExpenses();

          // Show a chart of expenses grouped by month
          showChartByMonth(expenses);
        }
        break;
      case QUIT:
        showBigText("Bye...!");
        break;

      default:
        break;
    }
  }
};
main();
