# JS Expenses

This is a terminal application to record expenses.

## Tools

This application was developed using:

- Javascript
- Nodejs
- VSCode

## Execution

### 1. Install packages

    npm install

### 2. Execute application

    npm run dev

## Packages

- nedb - https://www.npmjs.com/package/nedb
- nedb-promises - https://www.npmjs.com/package/nedb-promises
- inquirer - https://www.npmjs.com/package/inquirer
- ervy - https://www.npmjs.com/package/ervy
- cli-table3 - https://www.npmjs.com/package/cli-table3
